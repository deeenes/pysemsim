Release History
------------------
This is a summary of the changelog.

0.2.0:
+++++++++++
* Python3 compatibility.

0.1.0:
+++++++++++
* First release of pysemsim, for testing.
