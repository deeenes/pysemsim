#!/bin/bash

rm -f src/*.pyc
rm -f src/pysemsim/*.pyc
rm -rf src/pysemsim.egg-info

sed -i 's/\([0-9]*\.[0-9]*\.\)\([0-9]*\)/echo \1$\(\(\2+1\)\)/ge' src/pysemsim/__version__

python setup.py sdist
