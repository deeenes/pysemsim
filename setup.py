#!/usr/bin/env python
# -*- coding: utf-8 -*-

#
#  This file is part of the `pysemsim` python module
#
#  Copyright (c) 2014-2017 - EMBL-EBI
#
#  File author(s): Dénes Türei (denes@ebi.ac.uk)
#
#  Distributed under the GNU GPLv3 License.
#  See accompanying file LICENSE.txt or copy at
#      http://www.gnu.org/licenses/gpl-3.0.html
#
#  Website: http://www.ebi.ac.uk/~denes
#

__revision__ = "$Id$"
import sys
import os
from setuptools import setup, find_packages

with open(os.path.join('src', 'pysemsim', '__version__'), 'r') as f:
    __version__ = f.read().strip()

metainfo = {
    'authors': {
    'Türei':('Dénes Türei','turei.denes@gmail.com'),
    },
    'version': __version__,
    'license': 'GPLv3',
    'download_url': ['https://bitbucket.org/deeenes/pysemsim'],
    'url': ['http://www.ebi.ac.uk/~denes/'],
    'description': 'Calculates graph based semantic similarity for Gene Ontology',
    'platforms': ['Linux', 'Unix', 'MacOSX', 'Windows'],
    'keywords': ['network', 'graph', 'protein', 'gene',
                 'gene ontology',
                 'semantic similarity'],
    'classifiers': [
    'Development Status :: 4 - Beta',
    'Intended Audience :: Developers',
    'Intended Audience :: Science/Research',
    'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
    'Natural Language :: English',
    'Operating System :: OS Independent',
    'Programming Language :: Python',
    'Topic :: Software Development :: Libraries :: Python Modules',
    'Topic :: Scientific/Engineering :: Bio-Informatics',
    'Topic :: Scientific/Engineering :: Information Analysis',
    'Topic :: Scientific/Engineering :: Mathematics']
}

with open('README.rst') as f:
    readme = f.read()
with open('HISTORY.rst') as f:
    history = f.read()

deps = ['python-igraph', 'numpy']

setup(
    name = 'pysemsim',
    version = __version__,
    maintainer = metainfo['authors']['Türei'][0],
    maintainer_email = metainfo['authors']['Türei'][1],
    author = metainfo['authors']['Türei'][0],
    author_email = metainfo['authors']['Türei'][1],
    long_description = readme + '\n\n' + history,
    keywords = metainfo['keywords'],
    description = metainfo['description'],
    license = metainfo['license'],
    platforms = metainfo['platforms'],
    url = metainfo['url'],
    download_url = metainfo['download_url'],
    classifiers = metainfo['classifiers'],
    # package installation
    package_dir = {'':'src'},
    packages = list(set(find_packages() + ['pysemsim'])),
    include_package_data = True,
    install_requires = deps
)
