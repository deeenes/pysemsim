#!/usr/bin/env python
# -*- coding: utf-8 -*-

#
#  This file is part of the `pysemsim` python module
#
#  Copyright (c) 2014-2017 - EMBL-EBI
#
#  File author(s): Dénes Türei (denes@ebi.ac.uk)
#
#  Distributed under the GNU GPLv3 License.
#  See accompanying file LICENSE.txt or copy at
#      http://www.gnu.org/licenses/gpl-3.0.html
#
#  Website: http://www.ebi.ac.uk/~denes
#

from __future__ import print_function
from future.utils import iteritems

import re
import sys
import imp
import igraph
import numpy as np
import pysemsim.progress as progress

__all__ = ['GraphBased', 'GOTrees']

class GOTrees(object):
    
    def __init__(self, fnameObo, aspects = ['P'], silent = False):
        self.aspects = set(aspects if type(aspects) is list else [aspects])
        self.fnameObo = fnameObo
        self.silent = silent
        self.dIgTrees = {}
        self.diRoots = {}
        self.dfMaxDepths = {}
        self.dllNeighbors = {}
        self.ddObsolate = {}
        self.ddDefs = {}
        self.tGodCache = {}
        self.dAspects = {
            'cellular_component': 'C',
            'biological_process': 'P',
            'molecular_function': 'F'
        }
        self.dAspNames = dict((b, a) for a, b in iteritems(self.dAspects))
        self.message('\n\t=== w e l c o m e   t o   p y s e m s i m ! ===&\n\n')
        self.message('\tPlease go for a tea while preprocessing...\n')
        self.load_obo()
        self.message('\n\tHurray! Preprocessing is ready!\n')
    
    def reload(self):
        modname = self.__class__.__module__
        mod = __import__(modname, fromlist=[modname.split('.')[0]])
        imp.reload(mod)
        new = getattr(mod, self.__class__.__name__)
        setattr(self, '__class__', new)
    
    def message(self, message, override_silent = False):
        if not self.silent or override_silent:
            sys.stdout.write(message)
            sys.stdout.flush()
    
    def load_obo(self):
        rego = re.compile(r'.*(GO:[0-9]{7}).*')
        reln = re.compile(r'[a-z_]+:\s(.+)')
        resq = re.compile(r'\[.*\]')
        ltTree = []
        go = None
        obst = False
        
        with open(self.fnameObo, 'r') as f:
            for l in f:
                if go is not None and aspt in self.aspects:
                    if not obst:
                        self.ddDefs[go] = {
                            'term': term,
                            'def': deff,
                            'asp': aspt
                        }
                        for rel in rels:
                            ltTree.append((rel[0], go, rel[1], aspt))
                    else:
                        self.ddObsolate[go] = {
                            'name': term,
                            'def': deff,
                            'asp': aspt
                        }
                if l.startswith('[Term]'):
                    go = None
                    term = None
                    deff = None
                    aspt = None
                    obst = False
                    rels = []
                elif l.startswith('id: GO'):
                    go = rego.match(l).groups()[0]
                elif l.startswith('name:'):
                    term = reln.match(l).groups()[0].strip()
                elif l.startswith('def:'):
                    deff = resq.sub('', reln.match(l).groups()[0]).replace('"', '').\
                        replace('OBSOLATE. ', '').strip()
                elif l.startswith('namespace') and 'ext' not in l:
                    aspt = self.dAspects[reln.match(l).groups()[0].strip()]
                elif l.startswith('is_a: GO'):
                    rels.append((rego.match(l).groups()[0], 'is_a'))
                elif l.startswith('relationship: part_of'):
                    rels.append((rego.match(l).groups()[0], 'part_of'))
                elif l.startswith('is_obsolate: true'):
                    obst = True

        for asp in self.aspects:
            self.dIgTrees[asp] = igraph.Graph.TupleList([i for i in ltTree \
                    if i[3] == asp], 
                directed = True, edge_attrs = ['rel', 'asp'])
        
        self._preprocess()
    
    def _preprocess(self):
        for asp, ig in iteritems(self.dIgTrees):
            ig.vs['term'] = [self.ddDefs[v['name']]['term'] for v in ig.vs]
            ig.vs['def'] = [self.ddDefs[v['name']]['def'] for v in ig.vs]
            self.find_roots()
            ig.vs['depth'] = [None for _ in ig.vs]
            llDepths = ig.get_shortest_paths(self.diRoots[asp])
            for path in llDepths:
                if len(path) > 0:
                    ig.vs[path[-1]]['depth'] = len(path) - 1
            ig.delete_vertices(v.index for v in ig.vs if v['depth'] is None)
            ig = ig.simplify(combine_edges = {'rel': lambda x: '_'.join(sorted(x))})
            self.find_roots()
            self.dfMaxDepths[asp] = max(ig.vs['depth'])
            ig.es['weight'] = [(ig.vs[e.source]['depth'] + ig.vs[e.target]['depth']) / \
                2.0 / self.max_depth(asp) for e in ig.es]
            ig.vs['ancestors'] = [{} for _ in ig.vs]
            self.dllNeighbors[asp] = [self.dIgTrees[asp].neighbors(v.index, mode = 'OUT') \
                for v in self.dIgTrees[asp].vs]
            self._find_ancestors(ig.vs[self.diRoots[asp]], asp)
    
    def find_roots(self):
        for asp, ig in iteritems(self.dIgTrees):
            self.diRoots[asp] = ig.vs.find(term = self.dAspNames[asp]).index
    
    def max_depth(self, aspect):
        return self.dfMaxDepths[aspect]
    
    def _find_ancestors(self, v, aspect, prg = None, depth = 0):
        llNeighbors = self.dllNeighbors[aspect]
        ig = self.dIgTrees[aspect]
        if not prg:
            prg = progress.Progress(ig.ecount(), 'Looking up ancestors', 1)
        v['ancestors'][v.index] = 0.0
        for child in llNeighbors[v.index]:
            child = ig.vs[child]
            prg.step(status = 'depth = %u, eid = %u' % \
                (depth, ig.get_eid(v.index, child.index)))
            eWeight = ig.es[ig.get_eid(v.index, child.index)]['weight']
            for ancVid, aWeight in iteritems(v['ancestors']):
                child['ancestors'][ancVid] = aWeight + eWeight \
                    if ancVid not in child['ancestors'] \
                        or child['ancestors'][ancVid] > aWeight + eWeight \
                    else child['ancestors'][ancVid]
            self._find_ancestors(child, aspect, prg, depth + 1)
        if depth == 0:
            prg.terminate()
            ig.vs['ancestor_vids'] = [set(vi['ancestors'].keys()) for vi in ig.vs]
    
    def lookup_tables(self, aspect = 'P', regenerate = False):
        
        if (
            hasattr(self, 'dlVidName') and
            aspect in self.dlVidName and
            hasattr(self, 'ddNameVid') and
            aspect in self.ddNameVid and
            not regenerate
        ):
            return None
        
        if not hasattr(self, 'dlVidName'):
            self.dlVidName = {}
        
        if not hasattr(self, 'ddNameVid'):
            self.ddNameVid = {}
            
        self.dlVidName[aspect] = self.dIgTrees[aspect].vs['name']
        self.ddNameVid[aspect] = dict(map(lambda v: (v['name'], v.index),
                                          self.dIgTrees[aspect].vs))
    
    def get_ancestors(self, term, aspect = 'P'):
        """
        For one GO term returns its ancestors.
        """
        self.lookup_tables(aspect = aspect)
        
        setGONotFound = set([])
        
        try:
            return set(map(lambda a: self.dlVidName[aspect][a],
                self.dIgTrees[aspect].vs[self.ddNameVid[aspect][term]]['ancestors']))
        except KeyError:
            setGONotFound.add(term)
        
        if len(setGONotFound):
            sys.stdout.write('\n\t:: Some terms could not be found.\n'\
                             '\t   Most likely they are obsolate or from an other ontology.\n'\
                             '\t   These are:\n\t   %s\n' % (', '.join(setGONotFound))
                        )


class GraphBased(GOTrees):
    
    def __init__(self, fnameObo, aspects = ['P'], silent = False):
        self._usage = '\tThis class calculates a graph based semantic similarity\n'\
            '\tas it was described in Alvarez & Yan, J. Bioinform. Comput. Biol.\n'\
            '\t9:6 (2011) 681-695.\n\n'\
            '\tTo calculate semantic similarity between 2 GO terms:\n\n'\
            '\t\t>>> go.semSim(\'GO:0048311\', \'GO:0000019\', aspect = \'P\')\n\n'\
            '\tAbove, `go` is a <class pysemsim.GraphBased> instance, and aspect\n'\
            '\tthe ontology tree by default `P` for `biological_process`, or you\n'\
            '\t might use `C` for `cellular_component` or `F` for `molecular_function`.\n\n'\
            '\tTo calculate BMA (best match average) of the semantic similarities\n'\
            '\tbetween 2 sets of GO terms, call like this:\n\n'\
            '\t\t>>> go.setSemSimBMA(goSet1, goSet2, aspect = \'P\')\n\n'\
            '\tWhere `goSet1` and `goSet2` are lists of GO terms.\n\n'\
            '\tDon`t forget, PyGO caches the result, causing speedup\n'\
            '\tas you do more calculations. So keep the same instance.\n\n'\
            '\tGood luck!\n\n'
        super(GraphBased, self).__init__(fnameObo,
                                         aspects = aspects,
                                         silent = silent)
        self.message('\tYou can start to calculate semantic similarities:\n\n')
        self.message(self._usage)
    
    def help(self):
        sys.stdout.write(self._usage)
        sys.stdout.flush()
    
    def semSim(self, go1, go2, aspect = 'P'):
        if go1 == go2:
            return 1.0
        ig = self.dIgTrees[aspect]
        sp = 999.0
        vidNca = None
        tGo = tuple(sorted([go1, go2]))
        if tGo in self.tGodCache:
            return self.tGodCache[tGo]
        v1 = ig.vs.find(name = go1)
        v2 = ig.vs.find(name = go2)
        if v1 == self.diRoots[aspect] or v2 == self.diRoots[aspect]:
            # return 0.0
            pass
        for vidAc in v1['ancestor_vids'] & v2['ancestor_vids']:
            thisWeight = v1['ancestors'][vidAc] + v1['ancestors'][vidAc]
            if thisWeight < sp:
                sp = thisWeight
                vidNca = vidAc
        if vidNca is None:
            sys.stdout.write('\t:: Could not find nearest common ancestor\n'\
                '\t   for %s and %s. This is not ok.\n' % (go1, go2))
        spsim = (sp / float(self.max_depth(aspect)) - 1.0)**2
        nca = ig.vs[vidNca]['depth'] / float(self.max_depth(aspect))
        ssa = (spsim + nca) / 2.0
        self.tGodCache[tGo] = ssa
        return ssa
    
    def setSemSim(self, goSet1, goSet2, aspect = 'P', method = 'BMA'):
        aSs = np.zeros(shape = [len(goSet1), len(goSet2)])
        for i, go1 in enumerate(goSet1):
            for j, go2 in enumerate(goSet2):
                aSs[i,j] = self.semSim(go1, go2, aspect)
        if method == 'MAX':
            return np.amax(aSs)
        elif method == 'AVG':
            return np.mean(aSs)
        else:
            return np.mean([np.mean(np.amax(aSs, axis = 0)), 
                np.mean(np.amax(aSs, axis = 1))])
    
    def setSemSimBMA(self, goSet1, goSet2, aspect = 'P'):
        return self.setSemSim(goSet1, goSet2, aspect, 'BMA')
    
    def setSemSimMAX(self, goSet1, goSet2, aspect = 'P'):
        return self.setSemSim(goSet1, goSet2, aspect, 'MAX')
    
    def setSemSimAVG(self, goSet1, goSet2, aspect = 'P'):
        return self.setSemSim(goSet1, goSet2, aspect, 'AVG')
