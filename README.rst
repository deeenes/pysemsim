pysemsim
########


:Python versions:
    pysemsim supported both in Python 2.7 and 3.x.
:Issues\, contribution:
    denes@ebi.ac.uk, turei.denes@gmail.com

**Pysemsim** is a Python package harboring two classes: **GOTrees**
and **GraphBased**.

* **GOTrees** preprocesses an ontology from ``obo`` format and
  provides efficient methods to look up ancestors and path lengths.

* **GraphBased** calculates a graph based semantic similarity
  as it has been described in Alvarez & Yan, J. Bioinform. Comput. Biol.
  9:6 (2011) 681-695. The *ld* component is not implemented yet, so
  the scores are calculated based on the *shortest path similarities*,
  and the *depth of nearest common ancestor*.

The calculation consists of preprocessing and caching to enhance speed.

Obtaining ontology data
=======================

You need to download a Gene Ontology ontology (obo) file, for example 
from here:

http://geneontology.org/page/download-ontology

Usage
=====

To initialize the class:

..  code:: python
    
    import pysemsim
    go = pysemsim.GraphBased('go.obo')

To calculate semantic similarity between 2 GO terms:

..  code:: python
    
    go.semSim('GO:0048311', 'GO:0000019', aspect = 'P')

Above, `go` is a <class GraphBased> instance, and aspect is the ontology tree
by default `P` for `biological_process`, or you might use `C` for
`cellular_component` or `F` for `molecular_function`.
To calculate BMA (best match average) of the semantic similarities
between 2 sets of GO terms, call like this:

..  code:: python
    
    go.setSemSimBMA(goSet1, goSet2, aspect = 'P')

Where `goSet1` and `goSet2` are lists of GO terms.
Instead of BMA, you may use MAX for get the largest, 
or AVG for the mean of score values compared between sets.

Don`t forget, pysemsim caches the result, causing speedup
as you do more calculations. So keep the same instance.

Installation
============

igraph C library
----------------

Python igraph is a Python interface to use the igraph C library. The C library should be installed. 

Directly from git
-----------------

..  code:: bash
    
    pip2 install git+https://bitbucket.org/deeenes/pysemsim.git

With pip
--------

Download the package from /dist, and install with pip:

.. code:: bash
    
    pip2 install pysemsim-x.y.z.tar.gz

Build source distribution
-------------------------

Clone the git repo, and run setup.py:

.. code:: bash
    
    python2 setup.py sdist

License
=======

`pysemsim` is available under GNU GPL v3 license. See the accompanying `LICENSE.txt` or here: https://www.gnu.org/licenses/gpl-3.0.html.
